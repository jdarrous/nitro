#!/bin/bash

if [ "$#" -lt 1 ]; then
  echo "usage: ./run_docker.sh <command> <params>*"
  exit
fi

cmd=$1

date

DOCKER_IMAGE="nitro:1-alpine"
DOCKER_NETWORK="nitro_private"


# script varaibles
CONF_CACHE=".cache.conf"
touch $CONF_CACHE
source $CONF_CACHE

# containers variable
SRC_DIR=$(dirname $PWD)
ROOT_DIR=$(pwd)
ETC_DIR=$(pwd)/etc

# system variables
CHUNK_SIZE=$((2**16)) # 64 Kb
CHUNK_SIZE=$((2**20)) # 1 Mb
CHUNK_SIZE=$((2**18)) # 256 Kb

storage_backend="dedup"

# chunk_scheduler="random"
chunk_scheduler="network-aware"

# -----------------------------------------------------------------------------


first_time_init() {

  docker network create --subnet=192.168.10.0/16 $DOCKER_NETWORK

  docker build -t $DOCKER_IMAGE -f build/Dockerfile build
  docker pull redis:alpine

  mkdir etc data logs results redis-data

  # download test image, and convert from qcow2 format to raw format
  wget http://download.cirros-cloud.net/0.3.4/cirros-0.3.4-i386-disk.img
  qemu-img convert cirros-0.3.4-i386-disk.img data/cirros.raw
  rm cirros-0.3.4-i386-disk.img
}

clean() {
  docker network rm $DOCKER_NETWORK
  docker image rm $DOCKER_IMAGE redis:alpine
  rm -rf etc data logs results redis-data $CONF_CACHE
}


prepare_config_files() {
  peers=$(python -c "print ','.join(['\"192.168.30.%d:6969\"' % (i+1,) \
                                     for i in range($nb_peers)])")
  for i in $(seq $nb_peers); do
    peer_id=$(($i-1))
    mkdir -p etc/$peer_id
    cfg_file="etc/$peer_id/nitro.yaml"
    cp ../nitro.yaml.template $cfg_file
    sed -i '' "s/<id>/$peer_id/" $cfg_file
    sed -i '' "s/<peers>/$peers/" $cfg_file
    sed -i '' "s/<concurrent_conn_count>/2/" $cfg_file
    sed -i '' "s/<ws_address_bind>/'0.0.0.0'/" $cfg_file
    sed -i '' "s/<ws_address_connect>/http:\/\/192.168.40.$i:8080/" $cfg_file
    sed -i '' "s/<storage_address>/192.168.20.$i/" $cfg_file
    sed -i '' "s/<storage_backend>/redis/" $cfg_file
    sed -i '' "s/<zmq_proxy_to_comm_endpoint>/192.168.30.$i:50007/" $cfg_file
    sed -i '' "s/<zmq_comm_to_proxy_endpoint>/192.168.30.$i:50008/" $cfg_file
    sed -i '' "s/<storage_method>/$storage_backend/" $cfg_file
    sed -i '' "s/<compression_level>/6/" $cfg_file
    sed -i '' "s/<chunk_scheduler>/$chunk_scheduler/" $cfg_file
    sed -i '' "s/<chunk_size>/$CHUNK_SIZE/" $cfg_file
    sed -i '' "s/<root_dir>/\/opt/" $cfg_file
  done
}

run_redis() {

  for i in $(seq $nb_peers); do
    REDIS_DATA_PATH=$(pwd)/redis-data/$i
    mkdir -p $REDIS_DATA_PATH
    rm -f $REDIS_DATA_PATH/*
    docker run \
      -d --name redis-$i  \
      -v $REDIS_DATA_PATH:/data/ \
      --net $DOCKER_NETWORK --ip 192.168.20.$i \
      redis:alpine redis-server --appendonly yes
  done
}


run_cassandra() {
  for i in $(seq $nb_peers); do
    docker run -d --name cassandra-$i \
      --net $DOCKER_NETWORK --ip 192.168.20.$i \
      cassandra
  done
  sleep 10
}


run_comm_layer() {

  for i in $(seq $nb_peers -1 1); do
    peer_id=$(($i-1))
    docker run --name=comm-$i \
      -v $SRC_DIR:/home \
      -v $ETC_DIR/$peer_id:/etc/nitro \
      -e LOG_CFG='nitro/logging.yaml' \
      --net $DOCKER_NETWORK --ip 192.168.30.$i \
      -itd $DOCKER_IMAGE python nitro/comm-driver.py
    sleep 0.5
  done
}

run_storage_backend() {

  for i in $(seq $nb_peers -1 1); do
    peer_id=$(($i-1))
    # --link cassandra-$i \
    docker run --name=storage-$i \
      -v /tmp:/tmp \
      -v $SRC_DIR:/home \
      -v $ROOT_DIR:/opt \
      -v $ETC_DIR/$peer_id:/etc/nitro \
      -e LOG_CFG='nitro/logging.yaml' \
      --net $DOCKER_NETWORK --ip 192.168.40.$i \
      -itd $DOCKER_IMAGE python nitro/driver.py
  done
}

run_api() {

  for i in $(seq $nb_peers); do
    peer_id=$(($i-1))
    docker run --name api-$i \
      -v $SRC_DIR:/home \
      -v $ROOT_DIR:/opt \
      -v $ETC_DIR/$peer_id:/etc/nitro \
      -e LOG_CFG='nitro/logging.yaml' \
      --net $DOCKER_NETWORK --ip 192.168.50.$i \
      -itd $DOCKER_IMAGE /bin/sh
  done
}

run_all_containers() {
  run_redis
  # run_cassandra
  run_comm_layer
  run_storage_backend
  run_api
}

rm_all_containers() {
  docker ps -qa -f "network=$DOCKER_NETWORK" | xargs docker rm -f
}

stop_storage_comm() {
  for p in $(seq $nb_peers); do
    docker kill --signal=INT storage-$p
  done
  for p in $(seq $nb_peers); do
    docker kill --signal=INT comm-$p
  done
}

restart_drivers() {
  for p in $(seq $nb_peers -1 1); do
    docker restart storage-$p comm-$p
  done
}

rm_drivers() {
  for p in $(seq $nb_peers -1 1); do
    docker rm -f storage-$p comm-$p
  done
}

reset() {
  for i in $(seq $nb_peers); do
    docker exec -it redis-$i redis-cli flushall
  done
  for i in $(seq $nb_peers); do
    docker exec -it api-$i python nitro/api.py reset #> /dev/null
  done
}

info() {
  for i in $(seq $nb_peers); do
    docker exec -it api-$i python nitro/api.py info #> /dev/null
  done
}

network_t() {
  for i in $(seq $nb_peers); do
    docker exec -it api-$i python nitro/api.py network
  done
}

log() {
  for i in $(seq $nb_peers); do
    docker logs storage-$i
    docker logs comm-$i
  done
}

_add_to_first_nb_retrieve_from_other() {
  nb=$1
  input_image=$2
  for i in $(seq 1 $nb); do
    docker exec api-$i python nitro/api.py add /opt/data/$input_image > /tmp/uuid
    if [ $? -ne 0 ]; then
      echo "ERROR while adding image" >&2
      exit 1
    fi
    uuid=$(grep "###" /tmp/uuid | cut -d' ' -f 2 | xargs | tr -d '\r')
    echo $uuid
  done
  sleep 4 # time to publish the fingerprints
  for i in $(seq $nb $nb_peers); do
    time docker exec api-$i \
          python nitro/api.py checkout $uuid /opt/data/$i/$(basename $input_image).out
    if [ $? -ne 0 ]; then
      echo "ERROR while checking-out image" >&2
      exit 1
    fi
    sleep 2.5 # time to publish the fingerprints
  done
  ls -lR data/
}


workload() {

  rm -rf logs/* results/*
  > results/nitro_stats.csv
  for i in $(seq $nb $nb_peers); do
    mkdir -p data/$i
    rm -rf data/$i/*
  done

  rm_all_containers
  run_all_containers
  _add_to_first_nb_retrieve_from_other 1 cirros.raw
}


networking_test_workload() {
  rm -rf logs/*
  rm_all_containers
  run_comm_layer
}

# -----------------------------------------------------------------------------


if [ "$cmd" == "init" ]; then
  first_time_init
elif [ "$cmd" == "clean" ]; then
  clean
elif [ "$cmd" == "up" ]; then
  if [ "$#" -ne 2 ]; then
    echo "The number of sites is missing!"
    exit 1
  fi
  nb_peers=$2
  echo "nb_peers=$2" > $CONF_CACHE
  prepare_config_files
elif [ "$cmd" == "run" ]; then
  run_all_containers
elif [ "$cmd" == "rm" ]; then
  rm_all_containers
elif [ "$cmd" == "restart" ]; then
  restart_drivers
elif [ "$cmd" == "stop" ]; then
  stop_storage_comm
elif [ "$cmd" == "hard-restart" ]; then
  rm_drivers
  run_comm_layer
  run_storage_backend
elif [ "$cmd" == "info" ]; then
  info
elif [ "$cmd" == "reset" ]; then
  reset
elif [ "$cmd" == "network_t" ]; then
  network_t
elif [ "$cmd" == "log" ]; then
  log
elif [ "$cmd" == "workload" ]; then
  # networking_test_workload
  workload
else
  echo Wrong Option: $cmd
fi
