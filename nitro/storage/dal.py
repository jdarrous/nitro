
from model.image import ChunksImage
from storage import io

class DAL():
    """ Data Access Layer

    Provide more comprehensive api for DB layer
    """

    def __init__(self, ds, chunk_size, compressor, hasher):
        self.ds = ds
        self.chunk_size = chunk_size
        self.compressor = compressor
        self.hasher = hasher


    def store_image(self, img_data):
        serialized_fp = self.serialize_fingerprints(img_data.fingerprints)
        self.ds.put(img_data.uuid, serialized_fp)
        self.ds.persist()

    def retrieve_image_by_uuid(self, img_uuid):
        serialized_fp = self.ds.get(img_uuid)
        fp = self.deserialize_fingerprints(serialized_fp)
        return ChunksImage(img_uuid, fp)


    def add_image(self, img_file):

        delete_after = False

        if img_file.endswith(".tar.gz"):
            import tarfile
            tar = tarfile.open(img_file)
            tar.extractall('/tmp/dataset')
            tar.close()
            img_file = '/tmp/' + img_file[:-7]
            delete_after = True

        if img_file.endswith(".zip"):
            io.decompress_file(img_file, '/tmp/d.raw', self.compressor)
            img_file = '/tmp/d.raw'
            delete_after = True

        img_block_gen = io.read_chunks_from_file(img_file, self.chunk_size)
        img_data = ChunksImage.new()
        img_data.fingerprints.extend(self.add_chunks(img_block_gen))
        self.store_image(img_data)

        if delete_after:
            import os
            os.remove(img_file)

        return img_data

    def _get_chunk_iter(self, img_data):
        for digest in img_data.fingerprints:
            yield self.get_chunk(digest)

    def checkout_image(self, img_data, out_file):
        io.write_chunks_to_file(out_file, self._get_chunk_iter(img_data))


    def is_image_exist(self, img_uuid):
        return self.ds.exists(img_uuid)

    def is_chunk_exist(self, fp):
        return self.ds.exists(fp)


    def add_chunk(self, chunk):
        h = self.hasher.hash(chunk)
        if not self.ds.exists(h):
            self.ds.put(h, self.compressor.compress(chunk))
        return h

    def add_chunks(self, chunks):
        hashes = map(self.add_chunk, chunks)
        self.ds.persist()
        return hashes

    def get_chunk(self, fp):
        return self.compressor.decompress(self.get_compressed_chunk(fp))

    def add_compressed_chunk(self, h, comp_chunk):
        if not self.ds.exists(h):
            self.ds.put(h, comp_chunk)
        return h

    def get_compressed_chunk(self, fp):
        return self.ds.get(fp)

    def repo_size(self):
        return self.ds.used_memory()

    def serialize_fingerprints(self, fps):
        for fp in fps:
            assert len(fp) == self.hasher.get_digest_size()
        return ''.join(fps)

    def deserialize_fingerprints(self, ser_fps):
        n = self.hasher.get_digest_size()
        return [ser_fps[i:i+n] for i in xrange(0, len(ser_fps), n)]
