
import random

from scheduler.chunks_scheduler import ChunkScheduler

class RandomScheduler(ChunkScheduler):
    """ Random chunk scheduler"""

    def schedule(self):

        patitions = {}
        for chunks in self.chunks:
            sites_idx = [site_id
                         for site_id, is_in in enumerate(self.chunks_mapping[chunks])
                         if is_in == 1]
            source_id = random.choice(sites_idx)
            if source_id not in patitions:
                patitions[source_id] = []
            patitions.get(source_id).append(chunks)

        return patitions
